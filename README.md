School Inventory module
=======================

![screenshot](https://gitlab.com/francoisjacquet/School_Inventory/raw/master/screenshot.png?inline=false)

https://www.rosariosis.org/school-inventory-module/

Version 0.9 - June, 2017

License GNU GPL v2

Author François Jacquet

DESCRIPTION
-----------
Manage and keep track of your school asset.

Organize and filter items per category, status, location & person.

Reference your items, their quantities and adjoin them comments plus a document or photo.

This module adds the _School Inventory_ program to the Resources module.

Translated in [French](https://www.rosariosis.org/fr/school-inventory-module/) & [Spanish](https://www.rosariosis.org/es/school-inventory-module/).

CONTENT
-------
Resources
- School Inventory

INSTALL
-------
Copy the `School_Inventory/` folder (if named `School_Inventory-master`, rename it) and its content inside the `modules/` folder of RosarioSIS.

Go to _School Setup > School Configuration > Modules_ and click "Activate".

Requires RosarioSIS 2.9.15+
